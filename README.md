# C47

The [C47 project](https://gitlab.com/rpncalculators/c43) is a fork of the [WP43 project](https://gitlab.com/rpncalculators/wp43), adapted to work on a standard Swiss Micros DM42 calculator with the standard keyboard layout.

The two shifted key positions (f and g) on the calculator are supported by applying the infamous cycling shift behaviour to the DM42's single shift key, which means a custom faceplate or overlay is all that is needed to convert the DM42 hardware into a C47 calculator. No key stickers are required on a DM42.

Building on the user experience of the legendary HP-42S, tapping into the unprecedented accuracy of the WP34S engine, and adding a quite few extra features of its own, the C47 is a thriving open source project aiming to make RPN calculators highly relevant for mathematicians, scientists and engineers of today.

C43 / C47: The C47 historically is based on the WP43, which was called WP43S before it was renamed to WP43. The C47 as such initially was forked and called WP43C, then renamed to C43, then renamed to C47. Many references in the source code and documents on GitLab still refer to any or all of the listed prior names! This makes no difference to the current (C47) state of the project.

The C47 Wiki can be found [here](https://gitlab.com/rpncalculators/c43/-/wikis/home).
The C47 Community Wiki, which every user can help to create, can be found [here](https://gitlab.com/h2x/c47-wiki/-/wikis/home).

The page where you can order the bezel to glue it on a DM42 to make it a C47 can be found [here](https://47calc.com).

A comprehensive reference to all functions of the C47 as PDF downloads can be found [here](https://47calc.com/documentation/combined/doc.html).
