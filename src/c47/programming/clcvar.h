// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file clcvar.h
 ***********************************************/
#if !defined(CLCVAR_H)
  #define CLCVAR_H

  void fnClCVar(uint16_t unusedButMandatoryParameter);
#endif // !CLCVAR_H
