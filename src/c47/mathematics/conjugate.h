// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file conjugate.h
 ***********************************************/
#if !defined(CONJUGATE_H)
  #define CONJUGATE_H

  void conjCplx(void);
  void fnConjugate(uint16_t unusedButMandatoryParameter);

#endif // !CONJUGATE_H
