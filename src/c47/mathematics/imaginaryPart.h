// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file imaginaryPart.h
 ***********************************************/
#if !defined(IMAGINARYPART_H)
  #define IMAGINARYPART_H

  void fnImaginaryPart(uint16_t unusedButMandatoryParameter);
#endif // !IMAGINARYPART_H
