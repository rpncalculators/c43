// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file factorial.h
 ***********************************************/
#if !defined(FACTORIAL_H)
  #define FACTORIAL_H

  void fnFactorial(uint16_t unusedButMandatoryParameter);
#endif // !FACTORIAL_H
