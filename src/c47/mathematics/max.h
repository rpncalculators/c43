// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file max.h
 ***********************************************/
#if !defined(MAX_H)
  #define MAX_H

  void fnMax(uint16_t unusedButMandatoryParameter);
#endif // !MAX_H
