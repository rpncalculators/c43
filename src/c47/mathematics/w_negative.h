// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file w_negative.h
 ***********************************************/
#if !defined(W_NEGATIVE_H)
  #define W_NEGATIVE_H

  void fnWnegative(uint16_t unusedButMandatoryParameter);
#endif // !W_NEGATIVE_H
