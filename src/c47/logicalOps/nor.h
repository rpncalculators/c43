// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file nor.h
 ***********************************************/
#if !defined(NOR_H)
  #define NOR_H

  void fnLogicalNor(uint16_t unusedButMandatoryParameter);
#endif // !NOR_H
