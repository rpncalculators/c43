// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file not.h
 ***********************************************/
#if !defined(NOT_H)
  #define NOT_H

  void fnLogicalNot(uint16_t unusedButMandatoryParameter);
#endif // !NOT_H
