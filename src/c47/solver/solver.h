// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file solver.h
 ***********************************************/
#if !defined(SOLVER_H)
  #define SOLVER_H

  #include "differentiate.h"
  #include "equation.h"
  #include "graph.h"
  #include "integrate.h"
  #include "isumprod.h"
  #include "solve.h"
  #include "sumprod.h"
  #include "tvm.h"
#endif // !SOLVER_H
