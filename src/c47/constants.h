// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file constants.h functions for constant management
 ***********************************************/
#if !defined(CONSTANTS_H)
  #define CONSTANTS_H


  void fnConstant(const uint16_t constant);
  void fnPi      (uint16_t unusedButMandatoryParameter);
#endif // !CONSTANTS_H
