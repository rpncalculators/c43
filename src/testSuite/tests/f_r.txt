;*************************************************************
;*************************************************************
;**                                                         **
;**                  CCDF of F distribution                 **
;**                                                         **
;*************************************************************
;*************************************************************
In: FL_SPCRES=0 FL_CPXRES=0 SD=0 RMODE=0 IM=2compl SS=4 WS=64
Func: fnF_R



;=======================================
; F_R(Long Integer) --> Real
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RM=LonI:"1" RN=LonI:"1" RX=LonI:"1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"1" RX=Real:"0.5"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RM=LonI:"1" RN=LonI:"1" RX=LonI:"2"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"2" RX=Real:"0.3918265520306072701708555592224309"


In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RM=LonI:"2" RN=LonI:"1" RX=LonI:"1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"1" RX=Real:"0.5773502691896257645091487805019575"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RM=LonI:"2" RN=LonI:"1" RX=LonI:"2"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"2" RX=Real:"0.4472135954999579392818347337462552"


In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RM=LonI:"5" RN=LonI:"3" RX=LonI:"1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"1" RX=Real:"0.5351452100063649697724815118609178"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RM=LonI:"5" RN=LonI:"3" RX=LonI:"2"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"2" RX=Real:"0.3015473626950758266469209665576794"



;=======================================
; F_R(Time) --> Error24
;=======================================



;=======================================
; F_R(Date) --> Error24
;=======================================



;=======================================
; F_R(String) --> Error24
;=======================================
In:  FL_ASLIFT=0 RX=Stri:"String test"
Out: EC=24 FL_ASLIFT=0 RX=Stri:"String test"



;=======================================
; F_R(Real Matrix) --> Error24
;=======================================



;=======================================
; F_R(Complex Matrix) --> Error24
;=======================================



;=======================================
; F_R(Short Integer) --> Error24
;=======================================



;=======================================
; F_R(Real) --> Real
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RM=Real:"1" RN=Real:"1" RX=Real:"1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"1" RX=Real:"0.5"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RM=Real:"1" RN=Real:"1" RX=Real:"2"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"2" RX=Real:"0.3918265520306072701708555592224309"


In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RM=Real:"2" RN=Real:"1" RX=Real:"1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"1" RX=Real:"0.5773502691896257645091487805019575"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RM=Real:"2" RN=Real:"1" RX=Real:"2"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"2" RX=Real:"0.4472135954999579392818347337462552"


In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RM=Real:"5" RN=Real:"3" RX=Real:"1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"1" RX=Real:"0.5351452100063649697724815118609178"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RM=Real:"5" RN=Real:"3" RX=Real:"2"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"2" RX=Real:"0.3015473626950758266469209665576794"



;=======================================
; F_R(Complex) --> Error24
;=======================================
