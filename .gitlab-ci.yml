default:
  image: ubuntu:24.04

stages:
  - build
  - test
  - upload
  - release

variables:
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/c47/${CI_COMMIT_TAG}"
  GIT_SUBMODULE_STRATEGY: recursive
  DEBIAN_FRONTEND: noninteractive

macOS:
  stage: build
  image: macos-14-xcode-15
  script:
    - HOMEBREW_NO_AUTO_UPDATE=1 HOMEBREW_NO_INSTALL_CLEANUP=1 HOMEBREW_NO_INSTALLED_DEPENDENTS_CHECK=1 brew install ccache ninja python@3.9 gtk+3 gmp pulseaudio
    - pip3 install --upgrade pip
    - python3 -m pip install --user meson
    - USER_BASE_BIN=$(python3 -m site --user-base)/bin 
    - export PATH="$USER_BASE_BIN:$PATH"  # Add local bin to PATH
    - make dist_macos
  artifacts:
    expire_in: 1 day
    paths:
      - c47-macos.zip
  tags:
    - saas-macos-medium-m1

.linux-script-before: &linux-script-before
  - apt-get update -qq
  - apt-get install -qq -y git zip
  - apt-get install -qq -y pkgconf build-essential ninja-build meson
  - apt-get install -qq -y cmake libgmp-dev libgtk-3-dev
  - apt-get install -qq -y xvfb

linux:
  stage: build
  before_script:
    - *linux-script-before
    - apt-get install -qq -y libpulse-dev
  script:
    - make XVFB=xvfb-run dist_linux
  artifacts:
    expire_in: 1 day
    paths:
      - c47-linux.zip
  tags:
    - saas-linux-small-amd64

windows:
  stage: build
  script:
    - C:\msys64\msys2_shell.cmd -defterm -mingw64 -no-start -here -c "make dist_windows"
  artifacts:
    expire_in: 1 day
    paths:
      - c47-windows.zip
  tags:
    - c47_windows

dmcp:
  stage: build
  before_script:
    - *linux-script-before
    - apt-get install -qq -y gcc-arm-none-eabi
  script:
    - make XVFB=xvfb-run dist_dmcp
  artifacts:
    expire_in: 1 day
    paths:
      - c47-dmcp.zip
  tags:
    - saas-linux-small-amd64

dmcp5:
  stage: build
  before_script:
    - *linux-script-before
    - apt-get install -qq -y gcc-arm-none-eabi
  script:
    - make XVFB=xvfb-run dist_dmcp5
  artifacts:
    expire_in: 1 day
    paths:
      - c47-dmcp5.zip
  tags:
    - saas-linux-small-amd64

dmcp5r47:
  stage: build
  before_script:
    - *linux-script-before
    - apt-get install -qq -y gcc-arm-none-eabi
  script:
    - make XVFB=xvfb-run dist_dmcp5r47
  artifacts:
    expire_in: 1 day
    paths:
      - r47-dmcp5.zip
  tags:
    - saas-linux-small-amd64

testSuite:
  stage: test
  before_script:
    - *linux-script-before
  script:
    - make test
  tags:
    - saas-linux-small-amd64

codeDocs:
  stage: test
  before_script:
    - *linux-script-before
    - apt-get install -qq -y doxygen python3-sphinx python3-breathe furo
  script:
    - make docs

upload:
  stage: upload
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file c47-dmcp.zip ${PACKAGE_REGISTRY_URL}/c47-dmcp-${CI_COMMIT_TAG}.zip
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file c47-dmcp5.zip ${PACKAGE_REGISTRY_URL}/c47-dmcp5-${CI_COMMIT_TAG}.zip
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file r47-dmcp5.zip ${PACKAGE_REGISTRY_URL}/r47-dmcp5-${CI_COMMIT_TAG}.zip
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file c47-macos.zip ${PACKAGE_REGISTRY_URL}/c47-macos-${CI_COMMIT_TAG}.zip
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file c47-linux.zip ${PACKAGE_REGISTRY_URL}/c47-linux-${CI_COMMIT_TAG}.zip
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file c47-windows.zip ${PACKAGE_REGISTRY_URL}/c47-windows-${CI_COMMIT_TAG}.zip
  tags:
    - saas-linux-small-amd64

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG =~ /^[0-9]{2}\.[0-9]{3}\.[0-9]{2}\.[0-9]{2}([abr][c0-9][0-9]?(\..*)?)?$/
  script:
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
        --assets-link "{\"name\":\"Windows Simulator\",\"url\":\"${PACKAGE_REGISTRY_URL}/c47-windows-${CI_COMMIT_TAG}.zip\"}" \
        --assets-link "{\"name\":\"Linux Simulator\",\"url\":\"${PACKAGE_REGISTRY_URL}/c47-linux-${CI_COMMIT_TAG}.zip\"}" \
        --assets-link "{\"name\":\"macOS Simulator\",\"url\":\"${PACKAGE_REGISTRY_URL}/c47-macos-${CI_COMMIT_TAG}.zip\"}" \
        --assets-link "{\"name\":\"C47 Firmware for DMCP  (DM42)\",\"url\":\"${PACKAGE_REGISTRY_URL}/c47-dmcp-${CI_COMMIT_TAG}.zip\"}" \
        --assets-link "{\"name\":\"C47 Firmware for DMCP5 (DM42n)\",\"url\":\"${PACKAGE_REGISTRY_URL}/c47-dmcp5-${CI_COMMIT_TAG}.zip\"}" \
        --assets-link "{\"name\":\"R47 Firmware for DMCP5 (DM42n)\",\"url\":\"${PACKAGE_REGISTRY_URL}/r47-dmcp5-${CI_COMMIT_TAG}.zip\"}" \
  tags:
    - saas-linux-small-amd64
